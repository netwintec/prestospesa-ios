﻿using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.Globalization;
using System.Net;
using System.Text;
using UIKit;

namespace PrestoSpesa
{
    public partial class ViewController : UIViewController
    {

        int CarrelliSelected;

        UIStoryboard storyboard;

        UIViewController up, dp, tp;

        NSTimer timer;
        bool theBool;

        float progress = 0;
        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.


            Console.WriteLine(View.Frame+"|"+ContentView.Frame + "|" + LoadView.Frame);

            var frame = ContentView.Frame;
            frame.Size = View.Frame.Size;
            ContentView.Frame = frame;

            Console.WriteLine(View.Frame + "|" + ContentView.Frame + "|" + LoadView.Frame);

            base.NavigationController.NavigationBarHidden = true;

            //Load.StartAnimating();
            ProgressPerc.Font = UIFont.FromName("OpenSans-CondensedBold", 16);

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }

            Console.WriteLine("VIEWCONTREOLLER:" + ContentView.Frame);

            up = storyboard.InstantiateViewController("One");
            dp = storyboard.InstantiateViewController("Two");
            tp = storyboard.InstantiateViewController("Three");

            UILabel txt = new UILabel(new CGRect(10,30,ContentView.Frame.Width -20, 20));
            txt.Text = "Quanti scontrini vuoi fare?";
            txt.Font = UIFont.FromName("OpenSans-CondensedBold",20);
            txt.TextColor = UIColor.FromRGB(78, 78, 78);
            txt.TextAlignment = UITextAlignment.Center;

            UILabel Subtxt = new UILabel(new CGRect(10, 52, ContentView.Frame.Width - 20, 18));
            Subtxt.Text = "1 Carrello = 1 Scontrino";
            Subtxt.Font = UIFont.FromName("OpenSans-CondensedBold", 17);
            Subtxt.TextColor = UIColor.FromRGB(78, 78, 78);
            Subtxt.TextAlignment = UITextAlignment.Center;

            float H = (float)(ContentView.Frame.Height - 150) / 3;

            float IH = H - 50;
            float IW = (IH*450)/200;

            int yy = 100;

            UIView C1 = new UIView(new CGRect(0, yy, ContentView.Frame.Width, H));
            C1.BackgroundColor = UIColor.FromRGB(27, 160, 153);

            UIImageView im1 = new UIImageView(new CGRect(0, 0, ContentView.Frame.Width, 30));
            im1.Image = UIImage.FromFile("ShadeDownFirst.png");

            UIImageView im2 = new UIImageView(new CGRect(0, H-30, ContentView.Frame.Width, 30));
            im2.Image = UIImage.FromFile("ShadeUpFirst.png");

            UIImageView imC1 = new UIImageView(new CGRect(C1.Frame.Width/2 - IW/2, 25,IW,IH));
            imC1.Image = UIImage.FromFile("x1carrello.png");

            C1.Add(im1);
            C1.Add(im2);
            C1.Add(imC1);

            yy += (int)H + 10;

            UIView C2 = new UIView(new CGRect(0, yy, ContentView.Frame.Width, H));
            C2.BackgroundColor = UIColor.FromRGB(150, 194, 46);

            UIImageView im3 = new UIImageView(new CGRect(0, 0, ContentView.Frame.Width, 30));
            im3.Image = UIImage.FromFile("ShadeDownSecond.png");

            UIImageView im4 = new UIImageView(new CGRect(0, H - 30, ContentView.Frame.Width, 30));
            im4.Image = UIImage.FromFile("ShadeUpSecond.png");

            UIImageView imC2 = new UIImageView(new CGRect(C2.Frame.Width / 2 - IW / 2, 25, IW, IH));
            imC2.Image = UIImage.FromFile("x2carrello.png");

            C2.Add(im3);
            C2.Add(im4);
            C2.Add(imC2);

            yy += (int)H + 10;

            UIView C3 = new UIView(new CGRect(0, yy, ContentView.Frame.Width, H));
            C3.BackgroundColor = UIColor.FromRGB(57,174,107);

            UIImageView im5 = new UIImageView(new CGRect(0, 0, ContentView.Frame.Width, 30));
            im5.Image = UIImage.FromFile("ShadeDownThird.png");

            UIImageView im6 = new UIImageView(new CGRect(0, H - 30, ContentView.Frame.Width, 30));
            im6.Image = UIImage.FromFile("ShadeUpThird.png");

            UIImageView imC3 = new UIImageView(new CGRect(C3.Frame.Width / 2 - IW / 2, 25, IW, IH));
            imC3.Image = UIImage.FromFile("x3carrello.png");

            C3.Add(im5);
            C3.Add(im6);
            C3.Add(imC3);


            ContentView.Add(txt);
            ContentView.Add(Subtxt);
            ContentView.Add(C1);
            ContentView.Add(C2);
            ContentView.Add(C3);


            UITapGestureRecognizer C1Tap = new UITapGestureRecognizer((s) =>
            {

                CarrelliSelected = 1;

                startDownload();

            });
            C1.UserInteractionEnabled = true;
            C1.AddGestureRecognizer(C1Tap);

            UITapGestureRecognizer C2Tap = new UITapGestureRecognizer((s) =>
            {

                CarrelliSelected = 2;

                startDownload();

            });
            C2.UserInteractionEnabled = true;
            C2.AddGestureRecognizer(C2Tap);

            UITapGestureRecognizer C3Tap = new UITapGestureRecognizer((s) =>
            {

                CarrelliSelected = 3;

                startDownload();

            });
            C3.UserInteractionEnabled = true;
            C3.AddGestureRecognizer(C3Tap);

            /******** FONT *************

                OpenSans-CondensedBold
                OpenSans-Bold
                OpenSans

            ****************************/


            /*
            var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }
            */


            

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public void startDownload()
        {

            LoadView.Alpha = 1;

            /* v1 

            //PRODUZIONE
            //var client = new RestClient("http://2.228.89.216");
            //DEVELOP
            var client = new RestClient("http://test.netwintec.com");
           

            var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

            request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

            var handle = client.ExecuteAsync(request, (s, e) =>
            {
                AppDelegate.Instance.timeout = false;
                timer.Invalidate();

                Console.WriteLine("SC:"+s.StatusCode);

                if (s.StatusCode == HttpStatusCode.OK)
                {

                    InvokeOnMainThread(() =>
                    {

                        string csv = s.Content;

                        //Console.WriteLine(1);

                        string[] Lines = csv.Split('\n');

                        //Console.WriteLine("2|" + Lines.Length);

                        for (int i = 0; i < Lines.Length - 1; i++)
                        {
                            try
                            {
                                string lines = Lines[i];
                                //Console.WriteLine(lines);
                                string[] Value = lines.Split(';');

                                //Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

                                string Id = Value[0];
                                string Tit = Value[1];
                                string PrApp = Value[2];

                                float Pr = float.Parse(PrApp);
                                //Console.WriteLine("PR:" + Pr);
                                if (Id != ".")
                                    AppDelegate.Instance.ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                            }
                            catch (Exception ee)
                            {

                                Errore(2);

                            }
                        }

                        Console.WriteLine(3);

                        Progress.Progress = 1;
                        ProgressPerc.Text = 100 + "%";
                        

                        if (CarrelliSelected == 1)
                        {
                            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                            this.NavigationController.PresentModalViewController(up, true);
                        }

                        if (CarrelliSelected == 2)
                        {
                            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                            this.NavigationController.PresentModalViewController(dp, true);
                        }

                        if (CarrelliSelected == 3)
                        {
                            this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                            this.NavigationController.PresentModalViewController(tp, true);
                        }

                    });

                }

                if (s.StatusCode == HttpStatusCode.Unauthorized)
                {
                    InvokeOnMainThread(() =>
                    {
                        Errore(1);
                    });
                }
                if (s.StatusCode != HttpStatusCode.Unauthorized && s.StatusCode != HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {
                        Errore(2);
                    });
                }

            });

            //theBool = false;
            progress = 0;

            timer = NSTimer.CreateRepeatingScheduledTimer(0.1, delegate {

                    progress += 0.15f;
                    if (progress <= 0.95)
                    {
                        Progress.Progress = (float)progress;
                        ProgressPerc.Text = (progress * 100) + "%";

                    }

            });*/

            
            /*AppDelegate.Instance.timeout = true;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(10*1000);
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        if (AppDelegate.Instance.timeout)
                        {
                            handle.Abort();
                        }
                    });
                }
                catch (Exception e)
                {
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }
            })).Start();*/

            /** V2 */

            AppDelegate.Instance.timeout = true;

            try
            {

                var webclient = new WebClient();

                webclient.DownloadDataCompleted += (s, e) => {

                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.timeout = false;
                    });

                    if (e.Cancelled)
                    {
                        Console.WriteLine("CANCELLED");
                    }
                    if (e.Error != null)
                    {
                        Console.WriteLine("ERROR" + e.Error.Message);

                        if (e.Error.Message.Contains("401"))
                        {
                            InvokeOnMainThread(() =>
                            {
                                Errore(1,null);
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {

                                LogException(e.Error);
                                Errore(2,e.Error.Message);
                            });
                        }

                    }
                    if (e.Error == null && !e.Cancelled)
                    {

                        bool parserror = false;
                        InvokeOnMainThread(() =>
                        {

                            var bytes = e.Result;
                            string result = System.Text.Encoding.UTF8.GetString(bytes);
                            //Console.WriteLine("Result" + result);

                            string csv = result;

                            Console.WriteLine(1);

                            string[] Lines = csv.Split('\n');

                            Console.WriteLine("2|" + Lines.Length);

                            for (int i = 0; i < Lines.Length - 1; i++)
                            {
								try
								{
									string lines = Lines[i];
									//Console.WriteLine(lines);
									string[] Value = lines.Split(';');

									//Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

									string Id = Value[0];
									string Tit = Value[1];
									string PrApp = Value[2];

									var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
									culture.NumberFormat.NumberDecimalSeparator = ",";
								
                                    float Pr = float.Parse(PrApp);
                                    //Console.WriteLine("PR:" + Pr);
                                    if (Id != ".")
                                        AppDelegate.Instance.ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                                }
                                catch (Exception ee)
                                {
                                    parserror = true;
                                    LogException(ee);
                                    
                                    Errore(2,"Decoding Error");

                                }
                            }

                            Console.WriteLine(3);
                            if (!parserror)
                            {
                                if (CarrelliSelected == 1)
                                {
                                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                                    this.NavigationController.PresentModalViewController(up, true);
                                }

                                if (CarrelliSelected == 2)
                                {
                                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                                    this.NavigationController.PresentModalViewController(dp, true);
                                }

                                if (CarrelliSelected == 3)
                                {
                                    this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                                    this.NavigationController.PresentModalViewController(tp, true);
                                }
                            }
                        });

                    }
                };

                webclient.DownloadProgressChanged += (sender, e) => {

                    var bR = e.BytesReceived / 1024;
                    var trtR = e.TotalBytesToReceive / 1024;
                    Console.WriteLine(bR + "/" + trtR + " kB");
                    Console.WriteLine(e.ProgressPercentage + "%");

                    InvokeOnMainThread(() =>
                    {
                        Progress.Progress = (float)e.ProgressPercentage/100f;
                        ProgressPerc.Text = e.ProgressPercentage + "%";
                        //pbD.Text = bR + "/" + trtR + " kB";

                    });
                };

                // DEVELOPMENT
                //var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
                //PRODUZIONE
                var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/read.php");


                var header = new WebHeaderCollection();
                header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
                webclient.Headers = header;


                webclient.DownloadDataAsync(url);
                

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(10 * 1000);
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            if (AppDelegate.Instance.timeout)
                            {
                                webclient.CancelAsync();
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                        });
                    }
                })).Start();

            }
            catch (Exception ee) { }


            

        }

        public void Errore(int i,string ex)
        {

            LoadView.Alpha = 0;

            if (i == 1)
            {
                var adErr = new UIAlertView("Errore",
                                    "Il mercato delle opportunità è chiuso impossibile scaricare prodotti",
                                    null, "OK", null);
                adErr.Show();
            }
            if (i == 2) { 
                var adErr = new UIAlertView("Errore",
                                    "Caricamento dati non riuscito riprovare\n"+ex,
                                    null, "OK", null);
                adErr.Show();
            }

        }


        public void LogException(Exception ex) {

            InvokeOnMainThread(() =>
            {
                var client = new RestClient("http://test.netwintec.com");

                string log = "{'Message':'" + ex.Message + "'," +
                             "'InnerException':'" + ex.InnerException + "'," +
                             "'StackTrace':'" + ex.StackTrace + "'," +
                             "'Source':'" + ex.Source + "'," +
                             "}";

                var request = new RestRequest("/prestospesa-backend/log.php?log=" + log, Method.GET);

                client.ExecuteAsync(request, (s, e) => { });
            });
        }
    }
}