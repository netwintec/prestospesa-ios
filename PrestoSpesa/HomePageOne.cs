﻿using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ToastIOS;
using UIKit;
using ZXing.Mobile;

namespace PrestoSpesa
{
    public partial class HomePageOne : UIViewController
    {
        public static string TXTCOLLI = "Tot. colli";
        public static string TXTSPESA = "Tot. spesa";

        int i = 0;

        UIColor CR1 = UIColor.FromRGB(27, 160, 153);

        public Dictionary<string, ArticoloObject> ArticoloDictionary;

        bool isOne = true;

        public UIView MainContainer;

        public UIView Carrello1;
        public UIView Connecter1;

        public UITableView tab1;
        TableSourceOne ts1;

        public UIView View1;
        public UIView ViewFine1;

        public UILabel TotColli1;
        public UILabel TotSpesa1;

        public UIView BtnScanFoto1;
        public UIView BtnScanMan1;
        public UIView BtnFine1;

        public int colli1;
        public float spesa1;

        CGSize size;

        MobileBarcodeScanner scanner;

        bool scanOpen = false;
        bool scanFineOpen = false;

        Timer timer;
        public HomePageOne(IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var frame = ContentView.Frame;
            frame.Size = View.Frame.Size;
            ContentView.Frame = frame;

            scanner = new MobileBarcodeScanner(this);

            ArticoloDictionary = AppDelegate.Instance.ArticoloDictionary;

            float W = (float)(ContentView.Frame.Width - 10);

            Load.StartAnimating();

            CGSize sizef = UIStringDrawing.StringSize("SCANSIONA CODICE A BARRE", UIFont.FromName("OpenSans-Bold", 10), new CGSize(ContentView.Frame.Width - 225, 2000));
            CGSize sizem = UIStringDrawing.StringSize("INSERISCI CODICE PRODOTTO", UIFont.FromName("OpenSans-Bold", 10), new CGSize(ContentView.Frame.Width - 225, 2000));

            // *** CREAZIONE BOTTONI *** //

            UIView ButtonView = new UIView(new CGRect(5, 10, ContentView.Frame.Width - 10, 60));

            Carrello1 = new UIView(new CGRect(0, 0, W, 55));
            Carrello1.BackgroundColor = CR1;

            CGSize size = UIStringDrawing.StringSize("1", UIFont.FromName("OpenSans-Bold", 21), new CGSize(W, 2000));

            UILabel tC1 = new UILabel(new CGRect(W / 2 - ((size.Width + 40) / 2), 27.5 - size.Height / 2, size.Width, size.Height));
            tC1.Text = "1";
            tC1.Font = UIFont.FromName("OpenSans-Bold", 21);
            tC1.TextColor = UIColor.White;
            tC1.BackgroundColor = UIColor.Clear;

            UIImageView iC1 = new UIImageView(new CGRect((W / 2 - ((size.Width + 40) / 2)) + size.Width + 5, 10, 35, 35));
            iC1.Image = UIImage.FromFile("ScanFine.png");

            Console.WriteLine(tC1.Frame + "\n" + iC1.Frame);

            Carrello1.Add(tC1);
            Carrello1.Add(iC1);

            Connecter1 = new UIView(new CGRect(0, 55, W, 5));
            Connecter1.BackgroundColor = CR1;

            ButtonView.Add(Carrello1);
            ButtonView.Add(Connecter1);

            ContentView.Add(ButtonView);

            // *** CREAZIONE MAIN CONTAINER *** //

            MainContainer = new UIView(new CGRect(5, 70, ContentView.Frame.Width - 10, ContentView.Frame.Height - 80));
            MainContainer.BackgroundColor = CR1;
            ContentView.Add(MainContainer);

            // *** CREAZIONE PRIMO CARRELLO *** //

            View1 = new UIView(new CGRect(5, 5, MainContainer.Frame.Width - 10, MainContainer.Frame.Height - 10));
            View1.BackgroundColor = UIColor.White;

            ts1 = new TableSourceOne((float)(View1.Frame.Width - 10), 1, this);

            tab1 = new UITableView(new CGRect(5, 5, View1.Frame.Width - 10, View1.Frame.Height - 170));
            tab1.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tab1.Source = ts1;
            tab1.SeparatorColor = UIColor.Clear;
            //tab1.BackgroundColor = UIColor.Black;
            Console.WriteLine("TAb" + tab1.Frame);


            View1.Add(tab1);

            UIView txtCon1 = new UIView(new CGRect(0, View1.Frame.Height - 165, View1.Frame.Width, 30));
            txtCon1.BackgroundColor = CR1;

            TotColli1 = new UILabel(new CGRect(5, 5, txtCon1.Frame.Width / 2 - 10, 20));
            TotColli1.Text = TXTCOLLI + " " + colli1;
            TotColli1.Font = UIFont.FromName("OpenSans", 13);
            TotColli1.TextColor = UIColor.White;

            TotSpesa1 = new UILabel(new CGRect(txtCon1.Frame.Width / 2 + 5, 5, txtCon1.Frame.Width / 2 - 10, 20));
            TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
            TotSpesa1.Font = UIFont.FromName("OpenSans", 13);
            TotSpesa1.TextColor = UIColor.White;
            TotSpesa1.TextAlignment = UITextAlignment.Right;

            txtCon1.Add(TotColli1);
            txtCon1.Add(TotSpesa1);

            View1.Add(txtCon1);

            BtnScanFoto1 = new UIView(new CGRect(10, View1.Frame.Height - 125, View1.Frame.Width - 140, 55));
            BtnScanFoto1.BackgroundColor = CR1;

            UIImageView isf1 = new UIImageView(new CGRect(5, 5, 45, 45));
            isf1.Image = UIImage.FromFile("ScanFoto.png");

            UILabel lsf1 = new UILabel(new CGRect(55, 27.5 - sizef.Height / 2, BtnScanFoto1.Frame.Width - 65, sizef.Height));
            lsf1.Text = "SCANSIONA CODICE A BARRE";
            lsf1.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsf1.TextColor = UIColor.White;
            lsf1.Lines = 0;

            BtnScanFoto1.Add(isf1);
            BtnScanFoto1.Add(lsf1);

            BtnScanMan1 = new UIView(new CGRect(10, View1.Frame.Height - 65, View1.Frame.Width - 140, 55));
            BtnScanMan1.BackgroundColor = CR1;

            UIImageView ism1 = new UIImageView(new CGRect(5, 5, 45, 45));
            ism1.Image = UIImage.FromFile("ScanMan.png");

            UILabel lsm1 = new UILabel(new CGRect(55, 27.5 - sizem.Height / 2, BtnScanFoto1.Frame.Width - 65, sizem.Height));
            lsm1.Text = "INSERISCI CODICE PRODOTTO";
            lsm1.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsm1.TextColor = UIColor.White;
            lsm1.Lines = 0;

            BtnScanMan1.Add(ism1);
            BtnScanMan1.Add(lsm1);

            BtnFine1 = new UIView(new CGRect(View1.Frame.Width - 100, View1.Frame.Height - 125, 90, 115));
            BtnFine1.BackgroundColor = CR1;

            UIImageView if1 = new UIImageView(new CGRect(10, 54, 70, 46));
            if1.Image = UIImage.FromFile("carrelloUno.png");

            size = UIStringDrawing.StringSize("Concludi\nSpesa", UIFont.FromName("OpenSans-Bold", 12), new CGSize(BtnFine1.Frame.Width - 10, 2000));
            Console.WriteLine(size.Height);

            UILabel lf1 = new UILabel(new CGRect(5, 10, BtnFine1.Frame.Width - 10, 40));
            lf1.Text = "CONCLUDI\nSPESA";
            lf1.Font = UIFont.FromName("OpenSans-Bold", 11);
            lf1.TextColor = UIColor.White;
            lf1.TextAlignment = UITextAlignment.Center;
            lf1.Lines = 2;

            BtnFine1.Add(if1);
            BtnFine1.Add(lf1);

            View1.Add(BtnScanFoto1);
            View1.Add(BtnScanMan1);
            View1.Add(BtnFine1);

            ViewFine1 = new UIView(new CGRect(0, 0, View1.Frame.Width, View1.Frame.Height));
            ViewFine1.BackgroundColor = CR1;
            ViewFine1.Alpha = 0;

            size = UIStringDrawing.StringSize("Conto Spesa Chiuso", UIFont.SystemFontOfSize(17), new CGSize(ViewFine1.Frame.Width - 20, 2000));

            UILabel FineTxt1 = new UILabel(new CGRect(10, ViewFine1.Frame.Height / 2 - size.Height / 2, ViewFine1.Frame.Width - 10, size.Height));
            FineTxt1.Text = "Conto Spesa Chiuso";
            FineTxt1.Font = UIFont.FromName("OpenSans-Bold", 17);
            FineTxt1.TextColor = UIColor.White;
            FineTxt1.TextAlignment = UITextAlignment.Center;
            FineTxt1.Lines = 0;

            ViewFine1.Add(FineTxt1);

            View1.Add(ViewFine1);

            MainContainer.Add(View1);



            // *** GESTIONE BOTTONI ***//

            UITapGestureRecognizer BtnScanFoto1Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResult(result, 1);
                else
                    scanOpen = false;
            });
            BtnScanFoto1.UserInteractionEnabled = true;
            BtnScanFoto1.AddGestureRecognizer(BtnScanFoto1Click);


            UITapGestureRecognizer BtnScanMan1Click = new UITapGestureRecognizer(async () =>
            {

                var alert = new UIAlertView
                {
                    AlertViewStyle = UIAlertViewStyle.PlainTextInput,
                    Title = "Articolo",
                    Message = "Inserisci il codice prodotto."
                };

                alert.GetTextField(0).Placeholder = "Codice prodotto";

                alert.GetTextField(0).ShouldEndEditing += (UITextField) => {

                    alert.GetTextField(0).ResignFirstResponder();
                    return true;
                };

                int button = await ShowAlertText(alert, "Ok", "Annulla");
                Console.WriteLine("Click" + button);

                if (button == 0)
                {
                    bool result = AddArticolo(alert.GetTextField(0).Text.ToUpper(), 1);

                    if (!result)
                    {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }
                }

            });
            BtnScanMan1.UserInteractionEnabled = true;
            BtnScanMan1.AddGestureRecognizer(BtnScanMan1Click);
            

            UITapGestureRecognizer BtnFine1Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanFineOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResultFine(result, 1);
                else
                    scanFineOpen = false;
            });
            BtnFine1.UserInteractionEnabled = true;
            BtnFine1.AddGestureRecognizer(BtnFine1Click);


            UITapGestureRecognizer BtnCarrello1Click = new UITapGestureRecognizer(() =>
            {

                if (!isOne)
                {

                    Connecter1.BackgroundColor = CR1;

                    MainContainer.BackgroundColor = CR1;

                    isOne = true;

                    View1.Alpha = 1;

                }

            });
            Carrello1.UserInteractionEnabled = true;
            Carrello1.AddGestureRecognizer(BtnCarrello1Click);


            timer = new Timer();
            timer.Interval = 10 * 60 * 1000;
            timer.Elapsed += (sender, e) =>
            {
                if (!scanFineOpen)
                {
                    InvokeOnMainThread(() =>
                    {

                        if (scanOpen)
                            scanner.Cancel();

                        LoadView.Alpha = 1;

                        Console.WriteLine("TIMER");

                        startDownload();

                    });
                }

            };
            timer.Enabled = true;
            timer.Start();

        }

        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                string id = result.Text;
                InvokeOnMainThread(() => {

                    bool result2 = AddArticolo(id, Indice);
                    
                    if (!result2)
                    {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }
                    else
                    {
                        AudioToolbox.SystemSound systemSound = new AudioToolbox.SystemSound(1005);
                        systemSound.PlaySystemSound();
                        Toast.MakeText("Prodotto inserito correttamente", Toast.LENGTH_SHORT).Show();
                    }
                });
            }
            else
            {

                //InvokeOnMainThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }


        }

        public bool AddArticolo(string id, int Indice)
        {

            if (ArticoloDictionary.ContainsKey(id))
            {
                Console.WriteLine("esiste");
                if (Indice == 1)
                {

                    bool GiaInserito = false;

                    foreach (var a in ts1.items)
                    {

                        if (a.id == id)
                        {
                            GiaInserito = true;
                            a.qnt++;
                        }
                    }

                    ArticoloObject ao = ArticoloDictionary[id];

                    if (!GiaInserito)
                    {

                        ts1.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));
                        ts1.element++;
                    }

                    InvokeOnMainThread(() =>
                    {
                        tab1.ReloadData();
                    });

                    spesa1 += ao.prezzo;
                    colli1++;

                    TotColli1.Text = TXTCOLLI + " " + colli1;
                    TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

                }

                return true;

            }
            else
            {
                return false;
            }

        }

        public void UpdateField(int indice, float prezzo, bool add)
        {

            if (indice == 1)
            {
                if (add)
                {
                    spesa1 += prezzo;
                    colli1++;
                }
                else
                {
                    spesa1 -= prezzo;
                    colli1--;
                }

                TotColli1.Text = TXTCOLLI + " " + colli1;
                TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
            }

            


        }

        public async void CheckRemove(ArticoloCarrello obj, int i)
        {

            int button = await ShowAlert("Rimozione", "Stai rimuovendo " + obj.titolo, "Continua", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                if (i == 1)
                {
                    ts1.RemoveArticle(obj, true, i);
                    UpdateField(i, obj.prezzo, false);
                }



            }
            else
            {
                if (i == 1)
                    ts1.RemoveArticle(obj, false, i);
                

            };

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }
        public static Task<int> ShowAlertText(UIAlertView alert, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();

            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public void startDownload()
        {
            //* v1
            //PRODUZIONE
            var client = new RestClient("http://2.228.89.216");
            //DEVELOP
            //var client = new RestClient("http://test.netwintec.com");

            var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

            request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");


            var handle = client.ExecuteAsync(request, (s, e) =>
            {
                AppDelegate.Instance.timeoutPage = true;

                //Console.WriteLine(s.StatusCode + "\nContent:\n" + s.Content);

                if (s.StatusCode == HttpStatusCode.OK)
                {

                    InvokeOnMainThread(() =>
                    {

                        string csv = s.Content;

                        //Console.WriteLine(1);

                        string[] Lines = csv.Split('\n');

                        //Console.WriteLine("2|" + Lines.Length);

                        for (int i = 0; i < Lines.Length - 1; i++)
                        {
                            try
                            {
                                string lines = Lines[i];
                                //Console.WriteLine(lines);
                                string[] Value = lines.Split(';');

                                //Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

                                string Id = Value[0];
                                string Tit = Value[1];
                                string PrApp = Value[2];

                                float Pr = float.Parse(PrApp);// PrApp.Replace(',', '.'));

                                if (Id != ".")
                                {
                                    if (ArticoloDictionary.ContainsKey(Id))
                                        ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
                                    else
                                        ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                                }

                            }
                            catch (Exception ee)
                            {

                            }
                        }

                        //Console.WriteLine(3);

                        spesa1 = 0;

                        foreach (var a in ts1.items)
                        {
                            a.prezzo = ArticoloDictionary[a.id].prezzo;

                            spesa1 += (a.prezzo * a.qnt);

                        }



                        TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

                        tab1.ReloadData();

                        LoadView.Alpha = 0;

                    });
                }
                else
                {

                    InvokeOnMainThread(() =>
                    {

                        LoadView.Alpha = 0;

                    });
                }

            });

            AppDelegate.Instance.timeoutPage = true;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        if (AppDelegate.Instance.timeoutPage)
                        {
                            handle.Abort();
                        }
                    });
                }
                catch (Exception e)
                {
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }
            })).Start();

            /** V2 

            AppDelegate.Instance.timeoutPage = true;

            try
            {

                var webclient = new WebClient();

                webclient.DownloadDataCompleted += (s, e) =>
                {

                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.timeoutPage = false;
                    });

                    if (e.Cancelled)
                    {
                        Console.WriteLine("CANCELLED");
                    }
                    if (e.Error != null)
                    {
                        Console.WriteLine("ERROR" + e.Error.Message);

                        if (e.Error.Message.Contains("401"))
                        {
                            InvokeOnMainThread(() =>
                            {
                                ts1.items.Clear();
                                ts1.element = 0;


                                tab1.ReloadData();

                                spesa1 = 0;
                                colli1 = 0;

                                TotColli1.Text = TXTCOLLI + " " + colli1;
                                TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

                                Load.Alpha = 0;
                                //Errore(1);
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {
                                Load.Alpha = 0;
                                //Errore(2);
                            });
                        }

                    }
                    if (e.Error == null && !e.Cancelled)
                    {
                        InvokeOnMainThread(() =>
                        {
                            var bytes = e.Result;
                            string result = System.Text.Encoding.UTF8.GetString(bytes);
                            //Console.WriteLine("Result" + result);

                            string csv = result;

                            //Console.WriteLine(1);

                            string[] Lines = csv.Split('\n');

                            //Console.WriteLine("2|" + Lines.Length);

                            for (int i = 0; i < Lines.Length - 1; i++)
                            {
                                try
                                {
                                    string lines = Lines[i];
                                    //Console.WriteLine(lines);
                                    string[] Value = lines.Split(';');

                                    //Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

                                    string Id = Value[0];
                                    string Tit = Value[1];
                                    string PrApp = Value[2];

                                    float Pr = float.Parse(PrApp);// PrApp.Replace(',', '.'));

                                    if (Id != ".")
                                    {
                                        if (ArticoloDictionary.ContainsKey(Id))
                                            ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
                                        else
                                            ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                                    }

                                }
                                catch (Exception ee)
                                {

                                }
                            }

                            //Console.WriteLine(3);

                            spesa1 = 0;

                            foreach (var a in ts1.items)
                            {
                                a.prezzo = ArticoloDictionary[a.id].prezzo;

                                spesa1 += (a.prezzo * a.qnt);

                            }

                            TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
       
                            tab1.ReloadData();
                   
                            LoadView.Alpha = 0;

                        });

                    }
                };

                // DEVELOPMENT
                //var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
                //PRODUZIONE
                var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/read.php");


                var header = new WebHeaderCollection();
                header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
                webclient.Headers = header;


                webclient.DownloadDataAsync(url);

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(30 * 1000);
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            if (AppDelegate.Instance.timeoutPage)
                            {
                                webclient.CancelAsync();
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                        });
                    }
                })).Start();

            }
            catch (Exception ee) { }*/
        }

        void HandleScanResultFine(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                string txt = result.Text;
                this.InvokeOnMainThread(() => {

                    Toast.MakeText("Operazione in corso\nAttendere...", Toast.LENGTH_SHORT).Show();
                    ChiudiCarrello(txt, Indice);
                });

            }
            else
            {

                //this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }


        }


        public void ChiudiCarrello(string txt, int indice)
        {

            string carrello = "";

            if (indice == 1)
            {
                foreach (var a in ts1.items)
                {

                    if (a.id.Length == 12)
                        carrello += a.id + " ";
                    else
                        carrello += a.id;

                    if (a.qnt > 0 && a.qnt < 10)
                        carrello += "000" + a.qnt;

                    if (a.qnt >= 10 && a.qnt < 100)
                        carrello += "00" + a.qnt;

                    if (a.qnt >= 100 && a.qnt < 1000)
                        carrello += "0" + a.qnt;

                    if (a.qnt >= 1000 && a.qnt < 10000)
                        carrello += a.qnt;

                    carrello += "\r\n";
                }
            }

            //*  v1
            //PRODUZIONE
            var client = new RestClient("http://2.228.89.216");
            //DEVELOP
            //var client = new RestClient("http://test.netwintec.com");

            var request = new RestRequest("/prestospesa-backend/write.php", Method.POST);

            request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

            request.AddParameter("codice_cassa", txt);
            request.AddParameter("spesa", carrello);

            var handle = client.ExecuteAsync(request, (s, e) =>
            {
                AppDelegate.Instance.timeoutChiusura = false;

                Console.WriteLine("CHIUSURA:" + s.StatusCode + "\nContent:\n" + s.Content);

                if (s.StatusCode == HttpStatusCode.OK)
                {

                    InvokeOnMainThread(() =>
                    {

                        if (indice == 1)
                        {
                            ViewFine1.Alpha = 1;
                        }

                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {

                        var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                        adErr.Show();

                    });
                }

            });

            AppDelegate.Instance.timeoutChiusura = true;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        if (AppDelegate.Instance.timeoutChiusura)
                        {
                            handle.Abort();
                        }
                    });
                }
                catch (Exception e)
                {
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }
            })).Start();

            /* v2 

            AppDelegate.Instance.timeoutChiusura = true;

            try
            {

                var webclient = new WebClient();

                webclient.UploadValuesCompleted += (s, e) =>
                {

                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.timeoutChiusura = false;
                    });

                    if (e.Cancelled)
                    {
                        Console.WriteLine("CANCELLED");
                    }
                    if (e.Error != null)
                    {
                        Console.WriteLine("ERROR" + e.Error.Message);

                        if (e.Error.Message.Contains("401"))
                        {
                            InvokeOnMainThread(() =>
                            {
                                var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                                adErr.Show();
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {
                                var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                                adErr.Show();
                            });
                        }

                    }
                    if (e.Error == null && !e.Cancelled)
                    {
                        InvokeOnMainThread(() =>
                        {
                            if (indice == 1)
                            {
                                ViewFine1.Alpha = 1;
                            }

                        });

                    }
                };

                // DEVELOPMENT
                //var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/write.php");
                //PRODUZIONE
                var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/write.php");


                var header = new WebHeaderCollection();
                header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
                webclient.Headers = header;

                //request.AddParameter("codice_cassa", txt);
                //request.AddParameter("spesa", carrello);

                string dataString = @"{'codice_cassa':'"+txt+"','spesa':'" + carrello + "'}";
                byte[] dataBytes = Encoding.UTF8.GetBytes(dataString);

                NameValueCollection parameter = new NameValueCollection();
                parameter.Add("codice_cassa", txt);
                parameter.Add("spesa", carrello);

                webclient.UploadValuesAsync(url, "POST", parameter);

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(30 * 1000);
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            if (AppDelegate.Instance.timeoutChiusura)
                            {
                                webclient.CancelAsync();
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                        });
                    }
                })).Start();

            }
            catch (Exception ee) { }*/

        }
    }
}