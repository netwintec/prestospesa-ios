﻿using System;
namespace PrestoSpesa
{
	public class ArticoloCarrello
	{

		public string id { get; set; }
		public int qnt { get; set; }
		public float prezzo { get; set; }
		public string titolo { get; set; }

		public ArticoloCarrello(string i,int q,float p,string t)
		{

			id = i;
			qnt = q;
			prezzo = p;
			titolo = t;
		}

	}
}

