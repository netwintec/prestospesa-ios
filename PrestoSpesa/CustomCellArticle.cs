﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace PrestoSpesa
{
    class CustomCellArticle : UITableViewCell
    {
        UILabel nome, prezzo, qnt;
        //UITextView qnt;
        UIImageView plus,minus;
        UIView container, separator;

        UIColor CR1 = UIColor.FromRGB(27, 160, 153);
        UIColor CR2 = UIColor.FromRGB(150, 194, 46);
        UIColor CR3 = UIColor.FromRGB(57, 174, 107);

        CGSize sizeN, sizeP;

        public CustomCellArticle(NSString cellId,string n,float p,int q, int r,UITableViewSource s , int c,int i) : base(UITableViewCellStyle.Default, cellId)
        {
            
            SelectionStyle = UITableViewCellSelectionStyle.None;
            //ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);

            plus = new UIImageView();
            plus.Image = UIImage.FromFile("Plus.png");

            UITapGestureRecognizer plusClick = new UITapGestureRecognizer(() =>
            {

                if(c == 1)
                {
                    (s as TableSourceOne).ArticleChange(true,r);
                }

                if (c == 2)
                {
                    (s as TableSourceTwo).ArticleChange(true, r);
                }

                if (c == 3)
                {
                    (s as TableSourceThree).ArticleChange(true, r);
                }

            });
            plus.UserInteractionEnabled = true;
            plus.AddGestureRecognizer(plusClick);

            minus = new UIImageView();
            minus.Image = UIImage.FromFile("Minus.png");

            UITapGestureRecognizer minusClick = new UITapGestureRecognizer(() =>
            {

                if (c == 1)
                {
                    (s as TableSourceOne).ArticleChange(false, r);
                }

                if (c == 2)
                {
                    (s as TableSourceTwo).ArticleChange(false, r);
                }

                if (c == 3)
                {
                    (s as TableSourceThree).ArticleChange(false, r);
                }

            });
            minus.UserInteractionEnabled = true;
            minus.AddGestureRecognizer(minusClick);

            container = new UIView();

            

            if (i == 1)
                container.BackgroundColor = CR1;
            if (i == 2)
                container.BackgroundColor = CR2;
            if (i == 3)
                container.BackgroundColor = CR3;

            separator = new UIView();
            separator.BackgroundColor = UIColor.FromRGB(78,78,78);

            //Console.WriteLine(ContentView.Frame.Width);

            sizeN = UIStringDrawing.StringSize(n, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width - 185, 2000));
            sizeP = UIStringDrawing.StringSize(p.ToString("F") + " €", UIFont.FromName("OpenSans", 11), new CGSize(ContentView.Frame.Width - 185, 2000));

            nome = new UILabel()
            {
                Font = UIFont.FromName("OpenSans", 14),
                TextColor = UIColor.Black,
                Lines = 0,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear,
                Text = n

            };

            prezzo = new UILabel()
            {
                Font = UIFont.FromName("OpenSans", 11),
                TextColor = UIColor.Black,
                Lines = 0,
                TextAlignment = UITextAlignment.Left,
                BackgroundColor = UIColor.Clear,
                Text = p.ToString("F") + " €" 

            };

            qnt = new UILabel()
            {
                Font = UIFont.FromName("OpenSans", 15),
                TextColor = UIColor.Black,
                //KeyboardType = UIKeyboardType.NumberPad,
                Lines = 0,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.White,
                Text = q.ToString()
            };

            /*qnt.ShouldEndEditing += (UITextField) => {

                qnt.ResignFirstResponder();
                return true;
            };*/

            container.Add(qnt);

            ContentView.AddSubviews(new UIView[] { nome,prezzo,minus,container,plus,separator });

        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            nfloat widht = ContentView.Frame.Width;
            nfloat height = ContentView.Frame.Height;

            Console.WriteLine(widht+"|"+ height);

            float tot = (float)(sizeN.Height + sizeP.Height + 3);

            nome.Frame = new CGRect(20, height/2 - tot/2, sizeN.Width, sizeN.Height);
            prezzo.Frame = new CGRect(20, (height / 2 - tot / 2)+ sizeN.Height+3, sizeP.Width, sizeP.Height);

            minus.Frame = new CGRect(widht-135 , 10, 40, 40);
            container.Frame = new CGRect(widht-95, 10, 40, 40);
            qnt.Frame = new CGRect(2, 2, 36, 36);
            plus.Frame = new CGRect(widht - 55, 10, 40, 40);

            separator.Frame = new CGRect(20, 59, widht - 40, 1);

        }
    }
}