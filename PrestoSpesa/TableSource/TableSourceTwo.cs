using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;

namespace PrestoSpesa
{
    class TableSourceTwo : UITableViewSource
    {

        public List<ArticoloCarrello> items = new List<ArticoloCarrello>();
        public int element;
        float Widht;
        public int indice;
        HomePageTwo super;

        public TableSourceTwo(float w, int i, HomePageTwo hp)
        {

            Widht = w;
            indice = i;
            element = items.Count;
            super = hp;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return element;
        }



        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {


            return 60;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;

            ArticoloCarrello dItem = this.items[row];

            NSString cellIdentifier = new NSString(row.ToString());
            CustomCellArticle cell = new CustomCellArticle(cellIdentifier, dItem.titolo, dItem.prezzo, dItem.qnt, row, this, 2, indice);

            return cell;
        }

        public void ArticleChange(bool add, int pos)
        {

            ArticoloCarrello dItem = this.items[pos];

            if (add)
            {
                dItem.qnt++;

                var a = super;
                a.UpdateField(indice, dItem.prezzo, true);

                if (indice == 1)
                    super.tab1.ReloadData();

                if (indice == 2)
                    super.tab2.ReloadData();
            }
            else
            {


                dItem.qnt--;

                var a = super;

                if (dItem.qnt == 0)
                {
                    a.CheckRemove(dItem, indice);
                }
                else
                {
                    a.UpdateField(indice, dItem.prezzo, false);
                }

                if (indice == 1)
                    super.tab1.ReloadData();

                if (indice == 2)
                    super.tab2.ReloadData();

            }

        }

        public void RemoveArticle(ArticoloCarrello obj, bool elimina, int i)
        {

            if (elimina)
            {
                items.Remove(obj);
                element--;

            }
            else
            {
                obj.qnt++;
            }

            if (i == 1)
                super.tab1.ReloadData();

            if (i == 2)
                super.tab2.ReloadData();
        }
    }
}