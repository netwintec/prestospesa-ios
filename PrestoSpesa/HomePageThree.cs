﻿using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ToastIOS;
using UIKit;
using ZXing.Mobile;

namespace PrestoSpesa
{
    public partial class HomePageThree : UIViewController
    {

        public static string TXTCOLLI = "Tot. colli";
        public static string TXTSPESA = "Tot. spesa";

        int i = 0;

        UIColor CR1 = UIColor.FromRGB(27, 160, 153);
        UIColor CR2 = UIColor.FromRGB(150, 194, 46);
        UIColor CR3 = UIColor.FromRGB(57, 174, 107);

        public Dictionary<string, ArticoloObject> ArticoloDictionary;

        bool isOne = true;
        bool isSecond = false;
        bool isThird = false;

        public UIView MainContainer;

        public UIView Carrello1, Carrello2, Carrello3;
        public UIView Connecter1, Connecter2, Connecter3;

        public UITableView tab1, tab2, tab3;
        TableSourceThree ts1, ts2, ts3;

        public UIView View1, View2, View3;
        public UIView ViewFine1, ViewFine2, ViewFine3;

        public UILabel TotColli1, TotColli2, TotColli3;
        public UILabel TotSpesa1, TotSpesa2, TotSpesa3;

        public UIView BtnScanFoto1, BtnScanFoto2, BtnScanFoto3;
        public UIView BtnScanMan1, BtnScanMan2, BtnScanMan3;
        public UIView BtnFine1, BtnFine2, BtnFine3;

        public int colli1, colli2, colli3;
        public float spesa1, spesa2, spesa3;

        CGSize size;

        MobileBarcodeScanner scanner;

        bool scanOpen = false;
        bool scanFineOpen = false;

        Timer timer;
        public HomePageThree (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var frame = ContentView.Frame;
            frame.Size = View.Frame.Size;
            ContentView.Frame = frame;

            scanner = new MobileBarcodeScanner(this);

            ArticoloDictionary = AppDelegate.Instance.ArticoloDictionary;

            float W = (float)(ContentView.Frame.Width - 20)/3;

            Load.StartAnimating();

            CGSize sizef = UIStringDrawing.StringSize("SCANSIONA CODICE A BARRE", UIFont.FromName("OpenSans-Bold", 10), new CGSize(ContentView.Frame.Width - 225, 2000));
            CGSize sizem = UIStringDrawing.StringSize("INSERISCI CODICE PRODOTTO", UIFont.FromName("OpenSans-Bold", 10), new CGSize(ContentView.Frame.Width - 225, 2000));

            // *** CREAZIONE BOTTONI *** //

            UIView ButtonView= new UIView(new CGRect(5,10, ContentView.Frame.Width - 10, 60));

            Carrello1 = new UIView(new CGRect(0,0,W,55));
            Carrello1.BackgroundColor = CR1;

            CGSize size = UIStringDrawing.StringSize("1", UIFont.FromName("OpenSans-Bold", 21), new CGSize(W, 2000));

            UILabel tC1 = new UILabel(new CGRect(W / 2 - ((size.Width + 40) / 2), 27.5 - size.Height/2, size.Width, size.Height));
            tC1.Text= "1";
            tC1.Font = UIFont.FromName("OpenSans-Bold", 21);
            tC1.TextColor=UIColor.White;
            tC1.BackgroundColor = UIColor.Clear;

            UIImageView iC1 = new UIImageView(new CGRect((W / 2 - ((size.Width + 40) / 2))+ size.Width + 5, 10, 35,35));
            iC1.Image = UIImage.FromFile("ScanFine.png");

            Console.WriteLine(tC1.Frame + "\n" + iC1.Frame);

            Carrello1.Add(tC1);
            Carrello1.Add(iC1);

            Connecter1 = new UIView(new CGRect(0, 55, W, 5));
            Connecter1.BackgroundColor = CR1;

            Carrello2 = new UIButton(new CGRect(W+5, 0, W, 55));
            Carrello2.BackgroundColor = CR2;

            size = UIStringDrawing.StringSize("2", UIFont.FromName("OpenSans-Bold", 21), new CGSize(W, 2000));

            UILabel tC2 = new UILabel(new CGRect(W / 2 - (size.Width + 40) / 2, 27.5 - size.Height / 2, size.Width, size.Height));
            tC2.Text = "2";
            tC2.Font = UIFont.FromName("OpenSans-Bold", 21);
            tC2.TextColor = UIColor.White;
            tC2.BackgroundColor = CR2;

            UIImageView iC2 = new UIImageView(new CGRect((W / 2 - (size.Width + 40) / 2) + size.Width + 5, 10, 35, 35));
            iC2.Image = UIImage.FromFile("ScanFine.png");

            Carrello2.Add(tC2);
            Carrello2.Add(iC2);

            Connecter2 = new UIView(new CGRect(W+5, 55, W, 5));
            Connecter2.BackgroundColor = UIColor.White;

            Carrello3 = new UIButton(new CGRect(2*W + 10, 0, W, 55));
            Carrello3.BackgroundColor = CR3;

            size = UIStringDrawing.StringSize("3", UIFont.FromName("OpenSans-Bold", 21), new CGSize(W, 2000));

            UILabel tC3 = new UILabel(new CGRect(W / 2 - (size.Width + 40) / 2, 27.5 - size.Height / 2, size.Width, size.Height));
            tC3.Text = "3";
            tC3.Font = UIFont.FromName("OpenSans-Bold", 21);
            tC3.TextColor = UIColor.White;
            tC3.BackgroundColor = CR3;

            UIImageView iC3 = new UIImageView(new CGRect((W / 2 - (size.Width + 40) / 2) + size.Width + 5, 10, 35, 35));
            iC3.Image = UIImage.FromFile("ScanFine.png");

            Carrello3.Add(tC3);
            Carrello3.Add(iC3);

            Connecter3 = new UIView(new CGRect(2*W + 10, 55, W, 5));
            Connecter3.BackgroundColor = UIColor.White;

            ButtonView.Add(Carrello1);
            ButtonView.Add(Connecter1);
            ButtonView.Add(Carrello2);
            ButtonView.Add(Connecter2);
            ButtonView.Add(Carrello3);
            ButtonView.Add(Connecter3);

            ContentView.Add(ButtonView);

            // *** CREAZIONE MAIN CONTAINER *** //

            MainContainer = new UIView(new CGRect(5, 70, ContentView.Frame.Width - 10,ContentView.Frame.Height - 80));
            MainContainer.BackgroundColor = CR1;
            ContentView.Add(MainContainer);

            // *** CREAZIONE PRIMO CARRELLO *** //

            View1 = new UIView(new CGRect(5, 5, MainContainer.Frame.Width - 10, MainContainer.Frame.Height - 10));
            View1.BackgroundColor = UIColor.White;

            ts1 = new TableSourceThree((float)(View1.Frame.Width - 10), 1, this);

            tab1 = new UITableView(new CGRect(5, 5, View1.Frame.Width-10, View1.Frame.Height - 170));
            tab1.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tab1.Source = ts1;
            tab1.SeparatorColor = UIColor.Clear;
            //tab1.BackgroundColor = UIColor.Black;
            Console.WriteLine("TAb" + tab1.Frame);


            View1.Add(tab1);

            UIView txtCon1 = new UIView(new CGRect(0, View1.Frame.Height- 165, View1.Frame.Width, 30));
            txtCon1.BackgroundColor = CR1;

            TotColli1 = new UILabel(new CGRect(5, 5, txtCon1.Frame.Width / 2 - 10,20));
            TotColli1.Text =  TXTCOLLI + " " + colli1;
            TotColli1.Font = UIFont.FromName("OpenSans", 13);
            TotColli1.TextColor = UIColor.White;

            TotSpesa1 = new UILabel(new CGRect(txtCon1.Frame.Width / 2 + 5 , 5, txtCon1.Frame.Width / 2 - 10, 20));
            TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
            TotSpesa1.Font = UIFont.FromName("OpenSans", 13);
            TotSpesa1.TextColor = UIColor.White;
            TotSpesa1.TextAlignment = UITextAlignment.Right;

            txtCon1.Add(TotColli1);
            txtCon1.Add(TotSpesa1);

            View1.Add(txtCon1);

            BtnScanFoto1 = new UIView(new CGRect(10, View1.Frame.Height - 125, View1.Frame.Width-140, 55));
            BtnScanFoto1.BackgroundColor = CR1;

            UIImageView isf1 = new UIImageView(new CGRect(5,5,45,45));
            isf1.Image = UIImage.FromFile("ScanFoto.png");

            UILabel lsf1 = new UILabel(new CGRect(55, 27.5-sizef.Height/2, BtnScanFoto1.Frame.Width - 65, sizef.Height));
            lsf1.Text = "SCANSIONA CODICE A BARRE";
            lsf1.Font = UIFont.FromName("OpenSans-Bold",10);
            lsf1.TextColor = UIColor.White;
            lsf1.Lines = 0;

            BtnScanFoto1.Add(isf1);
            BtnScanFoto1.Add(lsf1);

            BtnScanMan1 = new UIView(new CGRect(10, View1.Frame.Height - 65, View1.Frame.Width -140, 55));
            BtnScanMan1.BackgroundColor = CR1;

            UIImageView ism1 = new UIImageView(new CGRect(5, 5, 45, 45));
            ism1.Image = UIImage.FromFile("ScanMan.png");

            UILabel lsm1 = new UILabel(new CGRect(55, 27.5 - sizem.Height / 2, BtnScanFoto1.Frame.Width - 65, sizem.Height));
            lsm1.Text = "INSERISCI CODICE PRODOTTO";
            lsm1.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsm1.TextColor = UIColor.White;
            lsm1.Lines = 0;

            BtnScanMan1.Add(ism1);
            BtnScanMan1.Add(lsm1);

            BtnFine1 = new UIView(new CGRect(View1.Frame.Width-100, View1.Frame.Height - 125, 90, 115));
            BtnFine1.BackgroundColor = CR1;

            UIImageView if1 = new UIImageView(new CGRect(10, 54, 70, 46));
            if1.Image = UIImage.FromFile("carrelloUno.png");

            size = UIStringDrawing.StringSize("Concludi\nSpesa", UIFont.FromName("OpenSans-Bold", 12), new CGSize(BtnFine1.Frame.Width - 10, 2000));
            Console.WriteLine(size.Height);

            UILabel lf1 = new UILabel(new CGRect(5, 10, BtnFine1.Frame.Width - 10, 40));
            lf1.Text = "CONCLUDI\nSPESA";
            lf1.Font = UIFont.FromName("OpenSans-Bold", 11);
            lf1.TextColor = UIColor.White;
            lf1.TextAlignment = UITextAlignment.Center;
            lf1.Lines = 2;

            BtnFine1.Add(if1);
            BtnFine1.Add(lf1);

            View1.Add(BtnScanFoto1);
            View1.Add(BtnScanMan1);
            View1.Add(BtnFine1);

            ViewFine1 = new UIView(new CGRect(0, 0, View1.Frame.Width, View1.Frame.Height));
            ViewFine1.BackgroundColor = CR1;
            ViewFine1.Alpha = 0;

            size = UIStringDrawing.StringSize("Conto Spesa Chiuso", UIFont.SystemFontOfSize(17), new CGSize(ViewFine1.Frame.Width - 20, 2000));

            UILabel FineTxt1 = new UILabel(new CGRect(10, ViewFine1.Frame.Height / 2 - size.Height / 2, ViewFine1.Frame.Width - 10, size.Height));
            FineTxt1.Text = "Conto Spesa Chiuso";
            FineTxt1.Font = UIFont.FromName("OpenSans-Bold", 17);
            FineTxt1.TextColor = UIColor.White;
            FineTxt1.TextAlignment = UITextAlignment.Center;
            FineTxt1.Lines = 0;

            ViewFine1.Add(FineTxt1);

            View1.Add(ViewFine1);

            MainContainer.Add(View1);




            // *** CREAZIONE SECONDO CARRELLO *** //

            View2 = new UIView(new CGRect(5, 5, MainContainer.Frame.Width - 10, MainContainer.Frame.Height - 10));
            View2.BackgroundColor = UIColor.White;
            View2.Alpha = 0;

            ts2 = new TableSourceThree((float)(View2.Frame.Width - 10), 2, this);

            tab2 = new UITableView(new CGRect(5, 5, View2.Frame.Width - 10, View2.Frame.Height - 170));
            tab2.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tab2.Source = ts2;
            tab2.SeparatorColor = UIColor.Clear;
            //tab2.BackgroundColor = UIColor.Black;
            Console.WriteLine("TAb" + tab2.Frame);


            View2.Add(tab2);

            UIView txtCon2 = new UIView(new CGRect(0, View2.Frame.Height - 165, View2.Frame.Width, 30));
            txtCon2.BackgroundColor = CR2;

            TotColli2 = new UILabel(new CGRect(5, 5, txtCon2.Frame.Width / 2 - 10, 20));
            TotColli2.Text = TXTCOLLI + " " + colli2;
            TotColli2.Font = UIFont.FromName("OpenSans", 13);
            TotColli2.TextColor = UIColor.White;

            TotSpesa2 = new UILabel(new CGRect(txtCon2.Frame.Width / 2 + 5, 5, txtCon2.Frame.Width / 2 - 10, 20));
            TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";
            TotSpesa2.Font = UIFont.FromName("OpenSans", 13);
            TotSpesa2.TextColor = UIColor.White;
            TotSpesa2.TextAlignment = UITextAlignment.Right;

            txtCon2.Add(TotColli2);
            txtCon2.Add(TotSpesa2);

            View2.Add(txtCon2);

            BtnScanFoto2 = new UIView(new CGRect(10, View2.Frame.Height - 125, View2.Frame.Width - 140, 55));
            BtnScanFoto2.BackgroundColor = CR2;

            UIImageView isf2 = new UIImageView(new CGRect(5, 5, 45, 45));
            isf2.Image = UIImage.FromFile("ScanFoto.png");

            UILabel lsf2 = new UILabel(new CGRect(55, 27.5 - sizef.Height / 2, BtnScanFoto2.Frame.Width - 65, sizef.Height));
            lsf2.Text = "SCANSIONA CODICE A BARRE";
            lsf2.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsf2.TextColor = UIColor.White;
            lsf2.Lines = 0;

            BtnScanFoto2.Add(isf2);
            BtnScanFoto2.Add(lsf2);

            BtnScanMan2 = new UIView(new CGRect(10, View2.Frame.Height - 65, View2.Frame.Width - 140, 55));
            BtnScanMan2.BackgroundColor = CR2;

            UIImageView ism2 = new UIImageView(new CGRect(5, 5, 45, 45));
            ism2.Image = UIImage.FromFile("ScanMan.png");

            UILabel lsm2 = new UILabel(new CGRect(55, 27.5 - sizem.Height / 2, BtnScanFoto2.Frame.Width - 65, sizem.Height));
            lsm2.Text = "INSERISCI CODICE PRODOTTO";
            lsm2.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsm2.TextColor = UIColor.White;
            lsm2.Lines = 0;

            BtnScanMan2.Add(ism2);
            BtnScanMan2.Add(lsm2);

            BtnFine2 = new UIView(new CGRect(View2.Frame.Width - 100, View2.Frame.Height - 125, 90, 115));
            BtnFine2.BackgroundColor = CR2;

            UIImageView if2 = new UIImageView(new CGRect(10, 54, 70, 46));
            if2.Image = UIImage.FromFile("carrelloDue.png");

            size = UIStringDrawing.StringSize("Concludi\nSpesa", UIFont.FromName("OpenSans-Bold", 12), new CGSize(BtnFine1.Frame.Width - 10, 2000));
            Console.WriteLine(size.Height);

            UILabel lf2 = new UILabel(new CGRect(5, 10, BtnFine2.Frame.Width - 10, 40));
            lf2.Text = "CONCLUDI\nSPESA";
            lf2.Font = UIFont.FromName("OpenSans-Bold", 11);
            lf2.TextColor = UIColor.White;
            lf2.TextAlignment = UITextAlignment.Center;
            lf2.Lines = 2;

            BtnFine2.Add(if2);
            BtnFine2.Add(lf2);

            View2.Add(BtnScanFoto2);
            View2.Add(BtnScanMan2);
            View2.Add(BtnFine2);

            ViewFine2 = new UIView(new CGRect(0, 0, View2.Frame.Width, View2.Frame.Height));
            ViewFine2.BackgroundColor = CR2;
            ViewFine2.Alpha = 0;

            size = UIStringDrawing.StringSize("Conto Spesa Chiuso", UIFont.FromName("OpenSans-Bold", 17), new CGSize(ViewFine2.Frame.Width - 20, 2000));

            UILabel FineTxt2 = new UILabel(new CGRect(10, ViewFine2.Frame.Height / 2 - size.Height / 2, ViewFine2.Frame.Width - 10, size.Height));
            FineTxt2.Text = "Conto Spesa Chiuso";
            FineTxt2.Font = UIFont.FromName("OpenSans-Bold", 17);
            FineTxt2.TextColor = UIColor.White;
            FineTxt2.TextAlignment = UITextAlignment.Center;
            FineTxt2.Lines = 0;

            ViewFine2.Add(FineTxt2);

            View2.Add(ViewFine2);

            MainContainer.Add(View2);



            // *** CREAZIONE SECONDO CARRELLO *** //

            View3 = new UIView(new CGRect(5, 5, MainContainer.Frame.Width - 10, MainContainer.Frame.Height - 10));
            View3.BackgroundColor = UIColor.White;
            View3.Alpha = 0;

            ts3 = new TableSourceThree((float)(View3.Frame.Width - 10), 3, this);

            tab3 = new UITableView(new CGRect(5, 5, View3.Frame.Width - 10, View3.Frame.Height - 170));
            tab3.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            tab3.Source = ts3;
            tab3.SeparatorColor = UIColor.Clear;
            //tab3.BackgroundColor = UIColor.Black;
            Console.WriteLine("TAb" + tab3.Frame);


            View3.Add(tab3);

            UIView txtCon3 = new UIView(new CGRect(0, View3.Frame.Height - 165, View3.Frame.Width, 30));
            txtCon3.BackgroundColor = CR3;

            TotColli3 = new UILabel(new CGRect(5, 5, txtCon3.Frame.Width / 2 - 10, 20));
            TotColli3.Text = TXTCOLLI + " " + colli3;
            TotColli3.Font = UIFont.FromName("OpenSans", 13);
            TotColli3.TextColor = UIColor.White;

            TotSpesa3 = new UILabel(new CGRect(txtCon3.Frame.Width / 2 + 5, 5, txtCon3.Frame.Width / 2 - 10, 20));
            TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";
            TotSpesa3.Font = UIFont.FromName("OpenSans", 13);
            TotSpesa3.TextColor = UIColor.White;
            TotSpesa3.TextAlignment = UITextAlignment.Right;

            txtCon3.Add(TotColli3);
            txtCon3.Add(TotSpesa3);

            View3.Add(txtCon3);

            BtnScanFoto3 = new UIView(new CGRect(10, View3.Frame.Height - 125, View3.Frame.Width - 140, 55));
            BtnScanFoto3.BackgroundColor = CR3;

            UIImageView isf3 = new UIImageView(new CGRect(5, 5, 45, 45));
            isf3.Image = UIImage.FromFile("ScanFoto.png");

            UILabel lsf3 = new UILabel(new CGRect(55, 27.5 - sizef.Height / 2, BtnScanFoto3.Frame.Width - 65, sizef.Height));
            lsf3.Text = "SCANSIONA CODICE A BARRE";
            lsf3.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsf3.TextColor = UIColor.White;
            lsf3.Lines = 0;

            BtnScanFoto3.Add(isf3);
            BtnScanFoto3.Add(lsf3);

            BtnScanMan3 = new UIView(new CGRect(10, View3.Frame.Height - 65, View3.Frame.Width - 140, 55));
            BtnScanMan3.BackgroundColor = CR3;

            UIImageView ism3 = new UIImageView(new CGRect(5, 5, 45, 45));
            ism3.Image = UIImage.FromFile("ScanMan.png");

            UILabel lsm3 = new UILabel(new CGRect(55, 27.5 - sizem.Height / 2, BtnScanFoto3.Frame.Width - 65, sizem.Height));
            lsm3.Text = "INSERISCI CODICE PRODOTTO";
            lsm3.Font = UIFont.FromName("OpenSans-Bold", 10);
            lsm3.TextColor = UIColor.White;
            lsm3.Lines = 0;

            BtnScanMan3.Add(ism3);
            BtnScanMan3.Add(lsm3);

            BtnFine3 = new UIView(new CGRect(View3.Frame.Width - 100, View3.Frame.Height - 125, 90, 115));
            BtnFine3.BackgroundColor = CR3;

            UIImageView if3 = new UIImageView(new CGRect(10, 54, 70, 46));
            if3.Image = UIImage.FromFile("carrelloTre.png");

            size = UIStringDrawing.StringSize("Concludi\nSpesa", UIFont.FromName("OpenSans-Bold", 12), new CGSize(BtnFine3.Frame.Width - 10, 2000));
            Console.WriteLine(size.Height);

            UILabel lf3 = new UILabel(new CGRect(5, 10, BtnFine3.Frame.Width - 10, 40));
            lf3.Text = "CONCLUDI\nSPESA";
            lf3.Font = UIFont.FromName("OpenSans-Bold", 11);
            lf3.TextColor = UIColor.White;
            lf3.TextAlignment = UITextAlignment.Center;
            lf3.Lines = 2;

            BtnFine3.Add(if3);
            BtnFine3.Add(lf3);

            View3.Add(BtnScanFoto3);
            View3.Add(BtnScanMan3);
            View3.Add(BtnFine3);

            ViewFine3 = new UIView(new CGRect(0, 0, View3.Frame.Width, View3.Frame.Height));
            ViewFine3.BackgroundColor = CR3;
            ViewFine3.Alpha = 0;

            size = UIStringDrawing.StringSize("Conto Spesa Chiuso", UIFont.FromName("OpenSans-Bold", 17), new CGSize(ViewFine3.Frame.Width - 20, 2000));

            UILabel FineTxt3 = new UILabel(new CGRect(10, ViewFine3.Frame.Height / 2 - size.Height / 2, ViewFine3.Frame.Width - 10, size.Height));
            FineTxt3.Text = "Conto Spesa Chiuso";
            FineTxt3.Font = UIFont.FromName("OpenSans-Bold", 17);
            FineTxt3.TextColor = UIColor.White;
            FineTxt3.TextAlignment = UITextAlignment.Center;
            FineTxt3.Lines = 0;

            ViewFine3.Add(FineTxt3);

            View3.Add(ViewFine3);

            MainContainer.Add(View3);



            // *** GESTIONE BOTTONI ***//

            UITapGestureRecognizer BtnScanFoto1Click = new UITapGestureRecognizer(async()=>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResult(result, 1);
                else
                    scanOpen = false;
            });
            BtnScanFoto1.UserInteractionEnabled = true;
            BtnScanFoto1.AddGestureRecognizer(BtnScanFoto1Click);


            UITapGestureRecognizer BtnScanFoto2Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResult(result, 2);
                else
                    scanOpen = false;
            });
            BtnScanFoto2.UserInteractionEnabled = true;
            BtnScanFoto2.AddGestureRecognizer(BtnScanFoto2Click);


            UITapGestureRecognizer BtnScanFoto3Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResult(result, 3);
                else
                    scanOpen = false;
            });
            BtnScanFoto3.UserInteractionEnabled = true;
            BtnScanFoto3.AddGestureRecognizer(BtnScanFoto3Click);


            UITapGestureRecognizer BtnScanMan1Click = new UITapGestureRecognizer(async () =>
            {

                var alert = new UIAlertView
                {
                    AlertViewStyle = UIAlertViewStyle.PlainTextInput,
                    Title = "Articolo",
                    Message = "Inserisci il codice prodotto."
                };

                alert.GetTextField(0).Placeholder = "Codice prodotto";

                alert.GetTextField(0).ShouldEndEditing += (UITextField) => {

                    alert.GetTextField(0).ResignFirstResponder();
                    return true;
                };

                int button = await ShowAlertText(alert, "Ok", "Annulla");
                Console.WriteLine("Click" + button);

                if (button == 0)
                {
                    bool result = AddArticolo(alert.GetTextField(0).Text.ToUpper(), 1);

                    if (!result) {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }

                }

            });
            BtnScanMan1.UserInteractionEnabled = true;
            BtnScanMan1.AddGestureRecognizer(BtnScanMan1Click);

            UITapGestureRecognizer BtnScanMan2Click = new UITapGestureRecognizer(async () =>
            {

                var alert = new UIAlertView
                {
                    AlertViewStyle = UIAlertViewStyle.PlainTextInput,
                    Title = "Articolo",
                    Message = "Inserisci il codice prodotto."
                };

                alert.GetTextField(0).Placeholder = "Codice prodotto";

                alert.GetTextField(0).ShouldEndEditing += (UITextField) => {

                    alert.GetTextField(0).ResignFirstResponder();
                    return true;
                };

                int button = await ShowAlertText(alert, "Ok", "Annulla");
                Console.WriteLine("Click" + button);

                if (button == 0)
                {
                    bool result = AddArticolo(alert.GetTextField(0).Text.ToUpper(), 2);

                    if (!result)
                    {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }

                }

            });
            BtnScanMan2.UserInteractionEnabled = true;
            BtnScanMan2.AddGestureRecognizer(BtnScanMan2Click);


            UITapGestureRecognizer BtnScanMan3Click = new UITapGestureRecognizer(async () =>
            {

                var alert = new UIAlertView
                {
                    AlertViewStyle = UIAlertViewStyle.PlainTextInput,
                    Title = "Articolo",
                    Message = "Inserisci il codice prodotto."
                };

                alert.GetTextField(0).Placeholder = "Codice prodotto";

                alert.GetTextField(0).ShouldEndEditing += (UITextField) => {

                    alert.GetTextField(0).ResignFirstResponder();
                    return true;
                };

                int button = await ShowAlertText(alert, "Ok", "Annulla");
                Console.WriteLine("Click" + button);

                if (button == 0)
                {
                    bool result = AddArticolo(alert.GetTextField(0).Text.ToUpper(), 3);

                    if (!result)
                    {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }

                }

            });
            BtnScanMan3.UserInteractionEnabled = true;
            BtnScanMan3.AddGestureRecognizer(BtnScanMan3Click);


            UITapGestureRecognizer BtnFine1Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanFineOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResultFine(result, 1);
                else
                    scanFineOpen = false;
            });
            BtnFine1.UserInteractionEnabled = true;
            BtnFine1.AddGestureRecognizer(BtnFine1Click);


            UITapGestureRecognizer BtnFine2Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanFineOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResultFine(result, 2);
                else
                    scanFineOpen = false;
            });
            BtnFine2.UserInteractionEnabled = true;
            BtnFine2.AddGestureRecognizer(BtnFine2Click);


            UITapGestureRecognizer BtnFine3Click = new UITapGestureRecognizer(async () =>
            {

                scanner.UseCustomOverlay = false;

                var options = new MobileBarcodeScanningOptions();
                options.PossibleFormats = new List<ZXing.BarcodeFormat>() {
                    ZXing.BarcodeFormat.CODE_128
                };
                //We can customize the top and bottom text of the default overlay
                //scanner.TopText = "Tieni la camera davanti al barcode da scansionare";
                //scanner.BottomText = "Attendi che il barcode venga scansionato!";

                scanFineOpen = true;

                //Start scanning

                ZXing.Result result = null;

                new System.Threading.Thread(new System.Threading.ThreadStart(delegate
                {
                    while (result == null)
                    {
                        Console.WriteLine("AF");
                        scanner.AutoFocus();
                        System.Threading.Thread.Sleep(2000);
                    }
                })).Start();

                result = await scanner.Scan(options);

                if (result != null)
                    HandleScanResultFine(result, 3);
                else
                    scanFineOpen = false;
            });
            BtnFine3.UserInteractionEnabled = true;
            BtnFine3.AddGestureRecognizer(BtnFine3Click);


            UITapGestureRecognizer BtnCarrello1Click = new UITapGestureRecognizer(() =>
            {

                if (!isOne)
                {

                    Connecter1.BackgroundColor=CR1;
                    Connecter2.BackgroundColor=UIColor.White;
                    Connecter3.BackgroundColor= UIColor.White;

                    MainContainer.BackgroundColor = CR1;

                    isOne = true;
                    isSecond = false;
                    isThird = false;

                    View1.Alpha = 1;
                    View2.Alpha = 0;
                    View3.Alpha = 0;

                }

            });
            Carrello1.UserInteractionEnabled = true;
            Carrello1.AddGestureRecognizer(BtnCarrello1Click);

            UITapGestureRecognizer BtnCarrello2Click = new UITapGestureRecognizer(() =>
            {

                if (!isSecond)
                {

                    Connecter1.BackgroundColor = UIColor.White;
                    Connecter2.BackgroundColor = CR2;
                    Connecter3.BackgroundColor = UIColor.White;

                    MainContainer.BackgroundColor = CR2;

                    isOne = false;
                    isSecond = true;
                    isThird = false;

                    View1.Alpha = 0;
                    View2.Alpha = 1;
                    View3.Alpha = 0;

                }

            });
            Carrello2.UserInteractionEnabled = true;
            Carrello2.AddGestureRecognizer(BtnCarrello2Click);

            UITapGestureRecognizer BtnCarrello3Click = new UITapGestureRecognizer(() =>
            {

                if (!isThird)
                {

                    Connecter1.BackgroundColor = UIColor.White;
                    Connecter2.BackgroundColor = UIColor.White;
                    Connecter3.BackgroundColor = CR3;

                    MainContainer.BackgroundColor = CR3;

                    isOne = false;
                    isSecond = false;
                    isThird = true;

                    View1.Alpha = 0;
                    View2.Alpha = 0;
                    View3.Alpha = 1;

                }

            });
            Carrello3.UserInteractionEnabled = true;
            Carrello3.AddGestureRecognizer(BtnCarrello3Click);

            timer = new Timer();
            timer.Interval = 10 * 60 * 1000;
            timer.Elapsed += (sender, e) =>
            {
                if (!scanFineOpen)
                {
                    InvokeOnMainThread(() =>
                    {

                        if (scanOpen)
                            scanner.Cancel();

                        LoadView.Alpha=1;

                        Console.WriteLine("TIMER");

                        startDownload();

                    });
                }

            };
            timer.Enabled = true;
            timer.Start();

        }

        void HandleScanResult(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                string id = result.Text;
                InvokeOnMainThread(() => {

                    bool result2 = AddArticolo(id, Indice);

                    if (!result2)
                    {
                        var adErr = new UIAlertView("Errore",
                                    "Codice Errato o non presente",
                                    null, "OK", null);
                        adErr.Show();
                    }
                    else
                    {
                        AudioToolbox.SystemSound systemSound = new AudioToolbox.SystemSound(1005);
                        systemSound.PlaySystemSound();
                        Toast.MakeText("Prodotto inserito correttamente", Toast.LENGTH_SHORT).Show();
                    }
                });
            }
            else
            {

                //InvokeOnMainThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }


        }

        public bool AddArticolo(string id, int Indice)
        {

            if (ArticoloDictionary.ContainsKey(id))
            {
                Console.WriteLine("esiste");
                if (Indice == 1)
                {

                    bool GiaInserito = false;

                    foreach (var a in ts1.items)
                    {

                        if (a.id == id)
                        {
                            GiaInserito = true;
                            a.qnt++;
                        }
                    }

                    ArticoloObject ao = ArticoloDictionary[id];

                    if (!GiaInserito)
                    {

                        ts1.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));
                        ts1.element++;
                    }

                    InvokeOnMainThread(() =>
                    {
                        tab1.ReloadData();
                    });

                    spesa1 += ao.prezzo;
                    colli1++;

                    TotColli1.Text = TXTCOLLI + " " + colli1;
                    TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

                }

                if (Indice == 2)
                {

                    bool GiaInserito = false;

                    foreach (var a in ts2.items)
                    {

                        if (a.id == id)
                        {
                            GiaInserito = true;
                            a.qnt++;
                        }
                    }

                    ArticoloObject ao = ArticoloDictionary[id];

                    if (!GiaInserito)
                    {

                        ts2.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));
                        ts2.element++;
                    }

                    tab2.ReloadData();

                    spesa2 += ao.prezzo;
                    colli2++;

                    TotColli2.Text = TXTCOLLI + " " + colli2;
                    TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";

                }

                if (Indice == 3)
                {
                    bool GiaInserito = false;

                    foreach (var a in ts3.items)
                    {

                        if (a.id == id)
                        {
                            GiaInserito = true;
                            a.qnt++;
                        }
                    }

                    ArticoloObject ao = ArticoloDictionary[id];

                    if (!GiaInserito)
                    {

                        ts3.items.Add(new ArticoloCarrello(ao.id, 1, ao.prezzo, ao.titolo));
                        ts3.element++;
                    }

                    tab3.ReloadData();

                    spesa3 += ao.prezzo;
                    colli3++;

                    TotColli3.Text = TXTCOLLI + " " + colli3;
                    TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";

                }

                return true;
            }
            else
            {
                return false;
            }

        }

        public void UpdateField(int indice, float prezzo, bool add)
        {

            if (indice == 1)
            {
                if (add)
                {
                    spesa1 += prezzo;
                    colli1++;
                }
                else
                {
                    spesa1 -= prezzo;
                    colli1--;
                }

                TotColli1.Text = TXTCOLLI + " " + colli1;
                TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
            }

            if (indice == 2)
            {
                if (add)
                {
                    spesa2 += prezzo;
                    colli2++;
                }
                else
                {
                    spesa2 -= prezzo;
                    colli2--;
                }

                TotColli2.Text = TXTCOLLI + " " + colli2;
                TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";
            }

            if (indice == 3)
            {
                if (add)
                {
                    spesa3 += prezzo;
                    colli3++;
                }
                else
                {
                    spesa3 -= prezzo;
                    colli3--;
                }

                TotColli3.Text = TXTCOLLI + " " + colli3;
                TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";
            }
        }

        public async void CheckRemove(ArticoloCarrello obj, int i)
        {

            int button = await ShowAlert("Rimozione", "Stai rimuovendo " + obj.titolo, "Continua", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {
                if (i == 1)
                {
                    ts1.RemoveArticle(obj, true,i);
                    UpdateField(i, obj.prezzo, false);
                }

                if (i == 2)
                {
                    ts2.RemoveArticle(obj, true, i);
                    UpdateField(i, obj.prezzo, false);
                }

                if (i == 3)
                {
                    ts3.RemoveArticle(obj, true, i);
                    UpdateField(i, obj.prezzo, false);
                };
            }
            else
            {
                if (i == 1)
                    ts1.RemoveArticle(obj, false, i);

                if (i == 2)
                    ts2.RemoveArticle(obj, false, i);

                if (i == 3)
                    ts3.RemoveArticle(obj, false, i);
            };

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }
        public static Task<int> ShowAlertText(UIAlertView alert, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();

            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public void startDownload()
        {
             //v1
            //PRODUZIONE
            var client = new RestClient("http://2.228.89.216");
            //DEVELOP
            //var client = new RestClient("http://test.netwintec.com");

            var request = new RestRequest("/prestospesa-backend/read.php", Method.GET);

            request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

            var handle = client.ExecuteAsync(request, (s, e) =>
            {

                AppDelegate.Instance.timeoutPage = false; 

                //Console.WriteLine(s.StatusCode + "\nContent:\n" + s.Content);

                if (s.StatusCode == HttpStatusCode.OK)
                {

                    InvokeOnMainThread(() =>
                    {

                        string csv = s.Content;

                        //Console.WriteLine(1);

                        string[] Lines = csv.Split('\n');

                        //Console.WriteLine("2|" + Lines.Length);

                        for (int i = 0; i < Lines.Length - 1; i++)
                        {
                            try
                            {
                                string lines = Lines[i];
                                //Console.WriteLine(lines);
                                string[] Value = lines.Split(';');

                                //Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

                                string Id = Value[0];
                                string Tit = Value[1];
                                string PrApp = Value[2];

                                float Pr = float.Parse(PrApp);// PrApp.Replace(',', '.'));

                                if (Id != ".")
                                {
                                    if (ArticoloDictionary.ContainsKey(Id))
                                        ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
                                    else
                                        ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                                }

                            }
                            catch (Exception ee)
                            {

                            }
                        }

                        //Console.WriteLine(3);

                        spesa1 = 0;
                        spesa2 = 0;
                        spesa3 = 0;

                        foreach (var a in ts1.items)
                        {
                            a.prezzo = ArticoloDictionary[a.id].prezzo;

                            spesa1 += (a.prezzo * a.qnt);

                        }

                        foreach (var b in ts2.items)
                        {
                            b.prezzo = ArticoloDictionary[b.id].prezzo;

                            spesa2 += (b.prezzo * b.qnt);
                        }

                        foreach (var c in ts3.items)
                        {
                            c.prezzo = ArticoloDictionary[c.id].prezzo;

                            spesa3 += (c.prezzo * c.qnt);
                        }

                        TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
                        TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";
                        TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";

                        tab1.ReloadData();
                        tab2.ReloadData();
                        tab3.ReloadData();

                        LoadView.Alpha = 0;

                    });
                }
                else
                {

                    InvokeOnMainThread(() =>
                    {

                        LoadView.Alpha = 0;

                    });
                }

            });

            AppDelegate.Instance.timeoutPage = true;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        if (AppDelegate.Instance.timeoutPage)
                        {
                            handle.Abort();
                        }
                    });
                }
                catch (Exception e)
                {
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }
            })).Start();

            /** V2 

            AppDelegate.Instance.timeoutPage = true;

            try
            {

                var webclient = new WebClient();

                webclient.DownloadDataCompleted += (s, e) =>
                {

                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.timeoutPage = false;
                    });

                    if (e.Cancelled)
                    {
                        Console.WriteLine("CANCELLED");
                    }
                    if (e.Error != null)
                    {
                        Console.WriteLine("ERROR" + e.Error.Message);

                        if (e.Error.Message.Contains("401"))
                        {
                            InvokeOnMainThread(() =>
                            {
                                ts1.items.Clear();
                                ts1.element = 0;


                                tab1.ReloadData();

                                spesa1 = 0;
                                colli1 = 0;

                                TotColli1.Text = TXTCOLLI + " " + colli1;
                                TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";

                                ts2.items.Clear();
                                ts2.element = 0;


                                tab2.ReloadData();

                                spesa2 = 0;
                                colli2 = 0;

                                TotColli2.Text = TXTCOLLI + " " + colli2;
                                TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";


                                ts3.items.Clear();
                                ts3.element=0;
                            

                                tab3.ReloadData();

                                spesa3 = 0;
                                colli3 = 0;

                                TotColli3.Text = TXTCOLLI + " " + colli3;
                                TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";


                                Load.Alpha = 0;
                                //Errore(1);
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {
                                Load.Alpha = 0;
                                //Errore(2);
                            });
                        }

                    }
                    if (e.Error == null && !e.Cancelled)
                    {
                        InvokeOnMainThread(() =>
                        {
                            var bytes = e.Result;
                            string result = System.Text.Encoding.UTF8.GetString(bytes);
                            //Console.WriteLine("Result" + result);

                            string csv = result;

                            //Console.WriteLine(1);

                            string[] Lines = csv.Split('\n');

                            //Console.WriteLine("2|" + Lines.Length);

                            for (int i = 0; i < Lines.Length - 1; i++)
                            {
                                try
                                {
                                    string lines = Lines[i];
                                    //Console.WriteLine(lines);
                                    string[] Value = lines.Split(';');

                                    //Console.WriteLine(Value[0] + "|" + Value[1] + "|" + Value[2]);

                                    string Id = Value[0];
                                    string Tit = Value[1];
                                    string PrApp = Value[2];

                                    float Pr = float.Parse(PrApp);// PrApp.Replace(',', '.'));

                                    if (Id != ".")
                                    {
                                        if (ArticoloDictionary.ContainsKey(Id))
                                            ArticoloDictionary[Id] = new ArticoloObject(Id, Pr, Tit);
                                        else
                                            ArticoloDictionary.Add(Id, new ArticoloObject(Id, Pr, Tit));
                                    }

                                }
                                catch (Exception ee)
                                {

                                }
                            }

                            //Console.WriteLine(3);

                            spesa1 = 0;
                            spesa2 = 0;
                            spesa3 = 0;

                            foreach (var a in ts1.items)
                            {
                                a.prezzo = ArticoloDictionary[a.id].prezzo;

                                spesa1 += (a.prezzo * a.qnt);

                            }

                            foreach (var b in ts2.items)
                            {
                                b.prezzo = ArticoloDictionary[b.id].prezzo;

                                spesa2 += (b.prezzo * b.qnt);
                            }

                            foreach (var c in ts3.items)
                            {
                                c.prezzo = ArticoloDictionary[c.id].prezzo;

                                spesa3 += (c.prezzo * c.qnt);
                            }

                            TotSpesa1.Text = TXTSPESA + " " + spesa1.ToString("F") + "€";
                            TotSpesa2.Text = TXTSPESA + " " + spesa2.ToString("F") + "€";
                            TotSpesa3.Text = TXTSPESA + " " + spesa3.ToString("F") + "€";

                            tab1.ReloadData();
                            tab2.ReloadData();
                            tab3.ReloadData();

                            LoadView.Alpha = 0;

                        });

                    }
                };

                // DEVELOPMENT
                //var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/read.php");
                //PRODUZIONE
                var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/read.php");


                var header = new WebHeaderCollection();
                header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
                webclient.Headers = header;


                webclient.DownloadDataAsync(url);

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(10 * 1000);
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            if (AppDelegate.Instance.timeoutPage)
                            {
                                webclient.CancelAsync();
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                        });
                    }
                })).Start();

            }
            catch (Exception ee) { }*/

        }

        void HandleScanResultFine(ZXing.Result result, int Indice)
        {

            scanOpen = false;

            string msg = "";

            if (result != null && !string.IsNullOrEmpty(result.Text))
            {
                string txt = result.Text;
                this.InvokeOnMainThread(() => {

                    Toast.MakeText("Operazione in corso\nAttendere...", Toast.LENGTH_SHORT).Show();
                    ChiudiCarrello(txt, Indice);
                });
            }
            else
            {

                //this.RunOnUiThread(() => Toast.MakeText(this, msg, ToastLength.Short).Show());
            }


        }


        public void ChiudiCarrello(string txt, int indice)
        {


            string carrello="";

            if (indice == 1)
            {
                foreach(var a in ts1.items){

                    if (a.id.Length == 12)
                        carrello += a.id + " "; 
                    else
                        carrello += a.id;

                    if (a.qnt > 0 && a.qnt < 10)
                        carrello += "000" + a.qnt;

                    if (a.qnt >= 10 && a.qnt < 100)
                        carrello += "00" + a.qnt;

                    if (a.qnt >= 100 && a.qnt < 1000)
                        carrello += "0" + a.qnt;

                    if (a.qnt >= 1000 && a.qnt < 10000)
                        carrello += a.qnt;

                    carrello += "\r\n";
                }
            }

            if (indice == 2)
            {
                foreach (var a in ts2.items)
                {

                    if (a.id.Length == 12)
                        carrello += a.id + " ";
                    else
                        carrello += a.id;

                    if (a.qnt > 0 && a.qnt < 10)
                        carrello += "000" + a.qnt;

                    if (a.qnt >= 10 && a.qnt < 100)
                        carrello += "00" + a.qnt;

                    if (a.qnt >= 100 && a.qnt < 1000)
                        carrello += "0" + a.qnt;

                    if (a.qnt >= 1000 && a.qnt < 10000)
                        carrello += a.qnt;

                    carrello += "\r\n";
                }
            }

            if (indice == 3)
            {
                foreach (var a in ts3.items)
                {

                    if (a.id.Length == 12)
                        carrello += a.id + " ";
                    else
                        carrello += a.id;

                    if (a.qnt > 0 && a.qnt < 10)
                        carrello += "000" + a.qnt;

                    if (a.qnt >= 10 && a.qnt < 100)
                        carrello += "00" + a.qnt;

                    if (a.qnt >= 100 && a.qnt < 1000)
                        carrello += "0" + a.qnt;

                    if (a.qnt >= 1000 && a.qnt < 10000)
                        carrello += a.qnt;

                    carrello += "\r\n";
                }
            }


            //* v1
            //PRODUZIONE
            var client = new RestClient("http://2.228.89.216");
            //DEVELOP
            //var client = new RestClient("http://test.netwintec.com");

            var request = new RestRequest("/prestospesa-backend/write.php", Method.POST);

            request.AddHeader("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");

            request.AddParameter("codice_cassa", txt);
            request.AddParameter("spesa", carrello);

            var handle = client.ExecuteAsync(request, (s, e) =>
            {
                AppDelegate.Instance.timeoutChiusura = false;

                Console.WriteLine("CHIUSURA:" + s.StatusCode + "\nContent:\n" + s.Content);

                if (s.StatusCode == HttpStatusCode.OK)
                {

                    InvokeOnMainThread(() =>
                    {

                        if (indice == 1)
                        {
                            ViewFine1.Alpha = 1;
                        }

                        if (indice == 2)
                        {
                            ViewFine2.Alpha = 1;
                        }

                        if (indice == 3)
                        {
                            ViewFine3.Alpha = 1;
                        }
                    });
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {

                        var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                        adErr.Show();

                    });
                }

            });
            

            AppDelegate.Instance.timeoutChiusura = true;

            new System.Threading.Thread(new System.Threading.ThreadStart(() =>
            {
                try
                {
                    System.Threading.Thread.Sleep(10 * 1000);
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        if (AppDelegate.Instance.timeoutChiusura)
                        {
                            handle.Abort();
                        }
                    });
                }
                catch (Exception e)
                {
                    AppDelegate.Instance.InvokeOnMainThread(() =>
                    {
                        Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                    });
                }
            })).Start();

            /* v2 

            AppDelegate.Instance.timeoutChiusura = true;

            try
            {

                var webclient = new WebClient();

                webclient.UploadValuesCompleted += (s, e) =>
                {

                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.timeoutChiusura = false;
                    });

                    if (e.Cancelled)
                    {
                        Console.WriteLine("CANCELLED");
                    }
                    if (e.Error != null)
                    {
                        Console.WriteLine("ERROR" + e.Error.Message);

                        if (e.Error.Message.Contains("401"))
                        {
                            InvokeOnMainThread(() =>
                            {
                                var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                                adErr.Show();
                            });
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {
                                var adErr = new UIAlertView("Errore",
                                    "Errore durante l'operazione di chiusura riprovare",
                                    null, "OK", null);
                                adErr.Show();
                            });
                        }

                    }
                    if (e.Error == null && !e.Cancelled)
                    {
                        InvokeOnMainThread(() =>
                        {
                            if (indice == 1)
                            {
                                ViewFine1.Alpha = 1;
                            }

                            if (indice == 2)
                            {
                                ViewFine2.Alpha = 1;
                            }

                            if (indice == 3)
                            {
                                ViewFine3.Alpha = 1;
                            }

                        });

                    }
                };

                // DEVELOPMENT
                //var url = new System.Uri("http://test.netwintec.com/prestospesa-backend/write.php");
                //PRODUZIONE
                var url = new System.Uri("http://app.gruppobonechi.it/prestospesa-backend/write.php");


                var header = new WebHeaderCollection();
                header.Add("access-token", "SCOj4qU5JB4ysjUKG1lDgR04hID4WU");
                webclient.Headers = header;

                //request.AddParameter("codice_cassa", txt);
                //request.AddParameter("spesa", carrello);

                string dataString = @"{'codice_cassa':'" + txt + "','spesa':'" + carrello + "'}";
                byte[] dataBytes = Encoding.UTF8.GetBytes(dataString);

                NameValueCollection parameter = new NameValueCollection();
                parameter.Add("codice_cassa", txt);
                parameter.Add("spesa", carrello);

                webclient.UploadValuesAsync(url, "POST", parameter);

                new System.Threading.Thread(new System.Threading.ThreadStart(() =>
                {
                    try
                    {
                        System.Threading.Thread.Sleep(10 * 1000);
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            if (AppDelegate.Instance.timeoutChiusura)
                            {
                                webclient.CancelAsync();
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        AppDelegate.Instance.InvokeOnMainThread(() =>
                        {
                            Console.WriteLine("Error getting the audio file\n" + e.StackTrace + "\n" + e.Message);
                        });
                    }
                })).Start();

            }
            catch (Exception ee) { }*/
        }
    }
}