﻿using System;
namespace PrestoSpesa
{
	public class ArticoloObject
	{
		public string id { get; set; }
		public float prezzo { get; set; }
		public string titolo { get; set; }

		public ArticoloObject(string i, float p, string t)
		{

			id = i;
			prezzo = p;
			titolo = t;
		}
	}
}

